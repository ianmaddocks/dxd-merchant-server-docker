FROM ubuntu 
RUN apt-get update -qq 
RUN apt-get upgrade -y
RUN apt-get install -y default-jre wget nodejs nano htop
RUN echo "export JAVA_HOME=/lib/jvm/default-java" >> /etc/profile

#maven
RUN wget -O maven.tgz https://downloads.apache.org/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz \
  && tar -xzf maven.tgz \
  && rm maven.tgz* \
  && mv apache-maven* /opt/maven
RUN echo "export PATH=$PATH:/opt/maven/bin" >> /etc/profile
ENV PATH="/opt/maven/bin:${PATH}"

#localtunnle (https channel)
RUN apt-get install -y npm 
RUN npm install -g localtunnel

#merchant-server
ENV MERCHANT_DOMAIN=dxd-merchant
ENV CLIENT_ID=1966A547-676E-4199-BABA-94920A855649
COPY merchant-spring-boot-reference-app /src/local/merchant-server/spring-app
WORKDIR /src/local/merchant-server
COPY .startup.sh .
COPY application-merchant.yml .
COPY start-server.sh .
RUN chmod +x .startup.sh
RUN chmod +x start-server.sh
VOLUME [/src/local/merchant-server]

ENTRYPOINT [ "/src/local/merchant-server/.startup.sh" ]
