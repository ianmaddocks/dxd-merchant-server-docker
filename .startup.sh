#!/bin/bash

lt --subdomain $MERCHANT_DOMAIN --port 8092 &
LT_PID=$! 
echo "local tunnel PID: "$LT_PID

#set enviroment vars into yml file
sed -i 's/client-id:.*/client-id: '$CLIENT_ID'/g' application-merchant.yml
sed -i 's/name: Merchant.*/name: Merchant '$CLIENT_ID'/g' application-merchant.yml
sed -i 's/gateway:.*/gateway: '$MERCHANT_DOMAIN'.loca.lt/g' application-merchant.yml
echo "Client ID: "$CLIENT_ID
echo "Gateway: https://"$MERCHANT_DOMAIN".loca.lt"

./start-server.sh

