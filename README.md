# Create docker image
1. run: '_git clone https://gitlab.com/mastercard/digital-by-default/merchant-spring-boot-reference-app.git_'
    * this will pull the Merchant Server source files, so they can be copied to the docker image
    * you will be asked to enter your gitlab username & password
2. run: '_docker build -t merchant-server ._'
    * this will create or update the docker image **merchant-server** using the **dockerfile** from this repository
    * the image is based off ubuntu
    * you can select an alternative docker image name but will need to carry forward the new image name to the steps below

# Running merchant server docker for the first time
1. create a new **CLEINT_ID** uuid
    * this must be a new value the first time you run the server **or each time you edit 'application-merchant.yml'
2. define your own **MERCHANT_DOMAIN** value, such as your name 'ian-maddocks'
    * This will be used in .startup.sh to create a localtunnel (https://{MERCHANT_DOMAIN}.loca.lt) with port 8092
3. run: '_docker run -it -v merchant_data:/src/local/merchant-server -e CLIENT_ID='070a464e-9b13-11eb-a8b3-0242ac130003' -e MERCHANT_DOMAIN='merchant-name' merchant-server_'
    * replacing:
        * '070a464e-9b13-11eb-a8b3-0242ac130003' with your **CLEINT_ID** uuid
        * 'merchant-name' with your **MERCHANT_DOMAIN** value        

Notes:
* if you build the docker image with an alternative image name, replace 'merchant-server' with the alternative name
* by default the merchant server uses Ian's hosted PSP server (https://psp-ian.loca.lt)
* the container starts by running '.startup.sh', which:
    * creates a localtunnel using your MERCHANT_DOMAIN value
    * copies CLEINT_ID and localtunnel url into 'application-merchant.yml'
    * runs 'start-server.sh', which:
        * copies 'application-merchant.yml' in to src/main/resources/, so it can be used by the maven application
        * starts maven merchant server application

# Running merchant server docker once tested
Once tested the following modifications could be made

## running in the background 
In the above the container runs in an interactive session, where the container stops when the interactive session is closed. Swapping augment '-it' to '-d' changes the container mode to detached, where the merchant server container will start and run silently and continually until the container is stopped.
1. run: '_docker run -d -v merchant_data:/src/local/merchant-server -e CLIENT_ID='070a464e-9b13-11eb-a8b3-0242ac130003' -e MERCHANT_DOMAIN='merchant-name' merchant-server_'

## connecting merchant server to an alternative PSP server
The 'application-merchant.yml' psp.id and psp.url define the PSP connection details
    * psp.id informs DXD which PSP server DXD should use for this merchant server registration
    * psp.url informs merchant server of the PSP inbound URL

To connect the merchant server to another DXD server:
1. add psp.id to merchant server configuration 'application-merchant.yml' file
    * retrieve the PSP server's recipientId, the value assigned by DXD when the PSP registers. 
        * PSP server's recipientId is  found in on the last line of the PSP's log, after registration and before processing activity. 
    * set psp.id to the PSP server's recipientId, in 'application-merchant.yml' file
2. add psp.url to merchant server configuration 'application-merchant.yml' file
    * retrieve the PSP server's communications-host.gateway
        * this is a value similar to the merchant server's communications-host.gateway which could be a localtunnel connection
    * set psp.url to the PSP server's URL, such as https://abc-xyz.loca.lt

## modify 'application-merchant.yml'
Edits to the 'application-merchant.yml' file can be incorporated by either:
1. edit yml file and:
    * '_docker stop merchant-server_'
    * '_docker rm merchant-server_'
    * '_docker build -t merchant-server ._'
    * '_docker run -d -v merchant_data:/src/local/merchant-server -e CLIENT_ID='070a464e-9b13-11eb-a8b3-0242ac130003' -e MERCHANT_DOMAIN='merchant-name' merchant-server_'
2. open bash session with running container and edit yml file with nano editor
    * '_docker exec -it merchant-server /bin/bash_'
    * docker session will open:
        * '_nano application-merchant.yml'
        * ctl-x, y, {enter}
        * '_exit_'
    * '_docker stop merchant-server_'
    * '_docker start merchant-server_'
3. mount container filesystem to docker host
    * edit application-merchant.yml
    * '_docker stop merchant-server_'
    * '_docker start merchant-server_'
