#!/bin/bash

cp application-merchant.yml spring-app/src/main/resources/

#start merchant server
cd spring-app
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dspring.profiles.active=merchant"
cd ..
kill $LT_PID